DROP TABLE IF EXISTS sueldo_basico;

CREATE TABLE sueldo_basico (
    id BIGINT IDENTITY PRIMARY KEY,
    fecha TIMESTAMP NOT NULL,
    funcion VARCHAR(500) NOT NULL,
    categoria1 DOUBLE,
    categoria2 DOUBLE,
    categoria3 DOUBLE,
    categoria4 DOUBLE
);
