package com.somospnt.suterh.repository;

import com.somospnt.suterh.domain.SueldoBasico;
import org.springframework.data.repository.CrudRepository;

public interface SueldoBasicoRepository extends CrudRepository<SueldoBasico, Long>{

}
