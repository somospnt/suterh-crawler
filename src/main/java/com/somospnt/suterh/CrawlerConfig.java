/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.suterh;

import com.altogamer.wdc.Processor;
import com.altogamer.wdc.WebCrawler;
import com.altogamer.wdc.impl.SimpleUrlContentRetriever;
import com.somospnt.suterh.wdc.SuterhCollector;
import com.somospnt.suterh.wdc.SuterhUrlIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CrawlerConfig {

    @Value("${com.somospnt.precios.crawler.poolsize}")
    private int poolSize;

    @Autowired
    private Processor processor;

    @Bean
    public WebCrawler suterh() {
        WebCrawler crawler = crearCrawler();
        crawler.setCollector(new SuterhCollector());
        crawler.setUrlIterator(new SuterhUrlIterator());
        crawler.setContentRetriever(new SimpleUrlContentRetriever());
        return crawler;
    }

    private WebCrawler crearCrawler() {
        WebCrawler crawler = new WebCrawler();
        crawler.setProcessor(processor);
        crawler.setCorePoolSize(poolSize);
        crawler.setMaximumPoolSize(poolSize);
        return crawler;
    }

}
