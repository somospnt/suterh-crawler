/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.suterh.wdc;

import com.altogamer.wdc.Collector;
import com.somospnt.suterh.domain.SueldoBasico;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SuterhCollector implements Collector {

    private static final Logger logger = LoggerFactory.getLogger(SuterhCollector.class);

    @Override
    public List<SueldoBasico> collect(String html) throws IOException {
        final List<SueldoBasico> results = new ArrayList<>();
        Date fecha = new Date();
        if (html.isEmpty()) {
            return results;
        }

        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(html);

        doc.$("div > table").each(new JerryFunction() {
            @Override
            public boolean onNode(Jerry $tabla, int index) {
                if (index == 0) {
                    $tabla.find("tbody > tr").each(new JerryFunction() {
                        @Override
                        public boolean onNode(Jerry $tr, int columna) {
                            SueldoBasico basico = new SueldoBasico();
                            basico.setFecha(fecha);
                            $tr.find("td").each(new JerryFunction() {
                                @Override
                                public boolean onNode(Jerry $td, int columna) {
                                    switch (columna) {
                                        case 0:
                                            basico.setFuncion($td.text());
                                            break;
                                        case 1:
                                            basico.setCategoria1(new BigDecimal($td.text()));
                                            break;
                                        case 2:
                                            basico.setCategoria2(new BigDecimal($td.text()));
                                            break;
                                        case 3:
                                            basico.setCategoria3(new BigDecimal($td.text()));
                                            break;
                                        case 4:
                                            basico.setCategoria4(new BigDecimal($td.text()));
                                            break;
                                        default:
                                            break;
                                    }
                                    if (columna == 4) {
                                        results.add(basico);
                                    }
                                    return true;
                                }
                            });
                            return true;
                        }
                    });
                }
                return true;
            }
        });

        return results;
    }


}
