/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.suterh.wdc;

import com.altogamer.wdc.Url;
import com.altogamer.wdc.UrlIterator;
import java.util.ArrayList;
import java.util.List;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.jerry.Jerry;

public class SuterhUrlIterator implements UrlIterator {

    private static final String URL = "http://www.suterh.org.ar";
    private List<String> urls = new ArrayList<>();
    private int index = 0;
    private int limit = -1;

    @Override
    public void init() {
        index = 0;
        urls.add(armarUrl(URL));

    }

    @Override
    public synchronized Url next() {
        if ((index >= urls.size() && limit < 0) || (limit >= 0 && index >= limit)) {
            return null;
        }
        String url = String.valueOf(urls.get(index));
        index++;
        return new Url(Url.Method.GET, url);
    }

    @Override
    public boolean allowEmptyResults() {
        return true;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    private String armarUrl(String url) {
        String nuevaUrl = url;
        HttpRequest request = HttpRequest.get(url);
        HttpResponse response = request.send();
        String html = response.bodyText();
        Jerry.JerryParser jerryParser = new Jerry.JerryParser();
        Jerry doc = jerryParser.parse(html);
        String planillaVigente = doc.$("body ul > li > a:contains(Planilla Vigente)").attr("href");
        nuevaUrl = nuevaUrl.concat(planillaVigente);
        return nuevaUrl;
    }

}
