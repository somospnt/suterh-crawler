package com.somospnt.suterh.wdc;

import com.altogamer.wdc.Processor;
import com.altogamer.wdc.Url;
import com.somospnt.suterh.repository.SueldoBasicoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SuterhProcessor implements Processor {

    @Autowired
    private SueldoBasicoRepository basicoRepository;

    @Override
    public void process(List items, Url url) {
        basicoRepository.save(items);
    }

}
