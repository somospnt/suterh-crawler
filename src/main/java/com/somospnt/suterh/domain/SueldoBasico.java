package com.somospnt.suterh.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;

@Entity
public class SueldoBasico implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fecha;
    private String funcion;
    private BigDecimal categoria1;
    private BigDecimal categoria2;
    private BigDecimal categoria3;
    private BigDecimal categoria4;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getFuncion() {
        return funcion;
    }

    public void setFuncion(String funcion) {
        this.funcion = funcion;
    }

    public BigDecimal getCategoria1() {
        return categoria1;
    }

    public void setCategoria1(BigDecimal categoria1) {
        this.categoria1 = categoria1;
    }

    public BigDecimal getCategoria2() {
        return categoria2;
    }

    public void setCategoria2(BigDecimal categoria2) {
        this.categoria2 = categoria2;
    }

    public BigDecimal getCategoria3() {
        return categoria3;
    }

    public void setCategoria3(BigDecimal categoria3) {
        this.categoria3 = categoria3;
    }

    public BigDecimal getCategoria4() {
        return categoria4;
    }

    public void setCategoria4(BigDecimal categoria4) {
        this.categoria4 = categoria4;
    }

}
