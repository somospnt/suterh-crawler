package com.somospnt.suterh.controller;

import com.somospnt.suterh.domain.SueldoBasico;
import com.somospnt.suterh.repository.SueldoBasicoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SueldoBasicoRestController {

    @Autowired
    private SueldoBasicoRepository basicoRepository;

    @RequestMapping("/sueldoBasico")
    public List<SueldoBasico> todos() {
        return (List<SueldoBasico>) basicoRepository.findAll();
    }

}
