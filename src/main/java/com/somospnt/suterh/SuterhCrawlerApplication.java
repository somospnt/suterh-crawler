package com.somospnt.suterh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SuterhCrawlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuterhCrawlerApplication.class, args);
    }
}
