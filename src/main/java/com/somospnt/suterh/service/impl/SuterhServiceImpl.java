package com.somospnt.suterh.service.impl;

import com.altogamer.wdc.WebCrawler;
import com.somospnt.suterh.domain.SueldoBasico;
import com.somospnt.suterh.repository.SueldoBasicoRepository;
import com.somospnt.suterh.service.SuterhService;
import com.somospnt.suterh.wdc.SuterhProcessor;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class SuterhServiceImpl implements SuterhService{

    @Autowired
    private WebCrawler crawler;

    @Scheduled(cron = "0 0 2 * * ?")
    @Override
    public void procesar() {
        try {
            crawler.run();
        } catch (IOException ex) {
            Logger.getLogger(SuterhServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(SuterhServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
