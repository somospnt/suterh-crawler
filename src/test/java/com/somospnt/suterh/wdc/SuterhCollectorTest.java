package com.somospnt.suterh.wdc;

import com.somospnt.suterh.domain.SueldoBasico;
import java.util.List;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class SuterhCollectorTest {

    private SuterhCollector suterhCollector;

     @Before
    public void setup() {
        suterhCollector = new SuterhCollector();

    }

    @Test
    public void testCollect() throws Exception {
        HttpRequest request = HttpRequest.get("http://www.suterh.org.ar/pagina/planilla-salarial-octubre-2014");
        HttpResponse response = request.send();
        String html = response.bodyText();
        List<SueldoBasico> result = suterhCollector.collect(html);
        assertFalse(result.isEmpty());
        assertTrue(result.size() > 20);
    }

}
