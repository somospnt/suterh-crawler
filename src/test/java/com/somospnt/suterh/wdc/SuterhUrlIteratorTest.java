/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package com.somospnt.suterh.wdc;

import com.altogamer.wdc.Url;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class SuterhUrlIteratorTest {

    private SuterhUrlIterator urlIterator;

    @Before
    public void setup() {
        urlIterator = new SuterhUrlIterator();
        urlIterator.init();
    }

    @Test
    public void next_hayMenues_devuelveUrlValidas() {
        int cantidad = 0;
        for (int i = 0; i < 30; i++) {
            Url url = urlIterator.next();
            if (url == null) break;
            System.out.println(url);
            cantidad++;
        }
        assertTrue("Debe haber 1 url", cantidad == 1);
    }

}
